from django.contrib.auth.models import User
from rest_framework import serializers
from .models import StockPortfolio,StockFolioUser,News

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']

class StockFolioUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = StockFolioUser
        fields = ['first_name','last_name','user','earnt','spent']

class StockPortfolioSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = StockPortfolio
        fields = ['user','stock','shares']

class NewsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = News
        fields = ['title','content','is_active','timestamp','updated']

