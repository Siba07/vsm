from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(StockFolioUser)
admin.site.register(StockPortfolio)
admin.site.register(News)
